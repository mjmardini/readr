# READR
A pdf book reader compatible with touch-screen devices with dark mode baked in.
Features include dark mode, line height, character spacing, font-size, and many more adjustments for clarity and comfort, reducing eye strain.

## TODO
- [ ] Handle image-based pdfs.
- [ ] Add svgs for sidebar buttons.
- [ ] Find best color schemes for mitigating eye strain and discomfort.
- [ ] More features...epub and other extensions.
- [ ] Add pictures to readme.
