import React from 'react';

export const ThemeContext = React.createContext();

export function ThemeContextProvider(props) {
  const [theme, setTheme] = React.useState({ isDark: true });

  function changeTheme() {
    setTheme({ isDark: !theme.isDark });
  }

  return (
    <ThemeContext.Provider value={{ theme, changeTheme }}>
      {props.children}
    </ThemeContext.Provider>
  );
}
