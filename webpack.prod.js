const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const merge = require('webpack-merge');
const common = require('./webpack.common');
const mini_css = require('mini-css-extract-plugin');
const optimize_css = require('optimize-css-assets-webpack-plugin');
const terser = require('terser-webpack-plugin');
const html_webpack_plugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = merge(common, {
  mode: 'production',
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'index.[contentHash].js',
    chunkFilename: 'chunk.[contentHash].js'
  },
  optimization: {
    minimizer: [
      new optimize_css(),
      new terser(),
      new html_webpack_plugin({
        template: './public/index.html',
        minify: {
          removeAttributeQuotes: true,
          collapseWhitespace: true,
          removeComments: true
        }
      })
    ]
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [mini_css.loader, 'css-loader']
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new mini_css({
      filename: '[name].[contentHash].css'
    }),
    new CopyWebpackPlugin({
      test: /(\.ico|\.)$/
    })
  ]
});
