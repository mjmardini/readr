const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common');
const html_webpack_plugin = require('html-webpack-plugin');

module.exports = merge(common, {
  mode: 'development',
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'index.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  plugins: [
    new html_webpack_plugin({
      template: './public/index.html'
    })
  ]
});
